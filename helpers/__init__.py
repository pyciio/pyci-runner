"""
Function helpers
"""


def config_from_object(obj):
    """
    Convert config object to dict
    """
    cfg = {}
    for key in dir(obj):
        if not key.startswith('__') and key.isupper():
            cfg[key] = getattr(obj, key)
    return cfg

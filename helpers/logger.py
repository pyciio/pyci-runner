"""
The format of the log library for simulating Logrus golang
https://github.com/sirupsen/logrus

Formatter supports color output of the log.
"""

import logging
from datetime import datetime
import six

BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)

COLORS = {
    # Colors by level
    'WARNING': YELLOW,
    'INFO': BLUE,
    'DEBUG': WHITE,
    'CRITICAL': YELLOW,
    'ERROR': RED,
}

SHORT_LEVELS = {
    'DEBUG': '[DEBU]',
    'INFO': '[INFO]',
    'WARNING': '[WARN]',
    'ERROR': '[ERRO]',
    'CRITICAL': '[CRIT]'
}
RESET_SEQ = "\033[0m"
COLOR_SEQ = "\033[%dm"
BOLD_SEQ = "\033[1m"


class LogrusFormatter(logging.Formatter):
    """Formatter class"""

    def __init__(self, colorize=True, *args, **kwargs):
        self.colorize = colorize
        # can't do super(...) here because Formatter is an old school class
        logging.Formatter.__init__(self, *args, **kwargs)

    def format(self, record):
        """
        Sets the color output (if colorize==True), and parse LogRecord with set
        fields.
        """
        levelname = record.levelname
        # Get level color
        lvl_color = COLOR_SEQ % (30 + COLORS[levelname])
        if self.colorize:
            attr_tmpl = "%s{0}%s={1}" % (lvl_color, RESET_SEQ)
            # Rename level
            record.levelname = lvl_color + SHORT_LEVELS[levelname] + RESET_SEQ
        else:
            attr_tmpl = "{0}={1}"
            # Rename level
            record.levelname = SHORT_LEVELS[levelname]
        # Add datetime attribute
        record.datetime = datetime.fromtimestamp(
            record.created).strftime("%Y-%m-%dT%H:%M:%S.%f")
        # Formattt and colorize other attributes
        # Attributes: levelno, lineno, process, thread convert from int to str
        types = six.string_types + six.integer_types
        for attr_name in dir(record):
            if not(attr_name.startswith('__') and attr_name.endswith('__')):
                if attr_name not in ['msg', 'args', 'exc_info', 'levelname']:
                    attr_val = getattr(record, attr_name)
                    if isinstance(attr_val, types):
                        val = attr_tmpl.format(attr_name, attr_val)
                        setattr(record, attr_name, val)
        # Set custom attributes
        if self.colorize:
            record.color = lvl_color
            record.color_reset = RESET_SEQ
            record.color_bold = BOLD_SEQ
        # make message
        message = logging.Formatter.format(self, record)
        if self.colorize:
            return message + RESET_SEQ
        else:
            return message

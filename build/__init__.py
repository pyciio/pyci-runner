"""
Build module to launch the obtained builds
"""

import os
import tempfile
import shutil
import logging
from multiprocessing.dummy import Pool
from pprint import PrettyPrinter
import git

from build.job import JobStatus, JobError
from build.job.container import Docker

LOG = logging.getLogger('pyci-worker')
PP = PrettyPrinter(indent=4)


class Build:  # pylint: disable=too-few-public-methods
    """Main class for run 'build' by config"""
    pipeline = None
    services = None

    def __init__(self, global_cfg_, cfg_):
        # Check required params in build config
        required = ["name", "repo", "stages", "pipeline"]
        if not all(p in cfg_ for p in required):
            raise Exception("required parameters: {0}".format(required))
        self.pipeline, self.services = self._parse_build_cfg(cfg_)
        self.cfg = cfg_
        self.global_cfg = global_cfg_

    def __enter__(self):
        # Clone git repo and change jobs config
        self._prepare()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # Remove services
        for _, srv in self.services.items():
            srv.remove()
            LOG.info("Service %s remove", srv.name)
        # Delete repo dir
        repo_dir = self.cfg.get("repo_dir")
        if repo_dir is not None:
            try:
                shutil.rmtree(repo_dir)
                LOG.info("Remove tmp dir %s", repo_dir)
            except OSError as err:
                LOG.error("Remove tmp dir error: %s", err)

    @staticmethod
    def _get_jobs(pipeline, stage):
        job_list = []
        for job_name, job_cfg in pipeline.items():
            if stage == job_cfg.get('stage', 'start'):
                job_list.append({'name': job_name, 'cfg': job_cfg})
        if len(job_list) == 0:
            raise Exception("Not found jobs for stage {0}".format(stage))
        return job_list

    def _parse_build_cfg(self, build_cfg):
        stages = build_cfg.get("stages")
        if not isinstance(stages, list):
            stages = [stages]
        pipeline = []
        services = {}
        for stage_name in stages:
            stage_jobs = self._get_jobs(build_cfg["pipeline"], stage_name)
            pipeline.append({"name": stage_name, "jobs": stage_jobs})
        for srv_aliace, srv_cfg in build_cfg.get('services', {}).items():
            srv_name = build_cfg["name"] + "-srv-" + srv_aliace
            services[srv_aliace] = Docker(srv_name, srv_cfg)
        return pipeline, services

    def _prepare(self):
        # Prepair git
        repo_dir = self.cfg.get("repo_dir")
        if repo_dir is None:
            repo_dir = tempfile.mkdtemp()
        self.cfg["repo_dir"] = repo_dir
        LOG.info("Pull repo %s to %s", self.cfg.get("repo"),
                 self.cfg.get("repo_dir"))
        git.Repo.clone_from(self.cfg.get("repo"), self.cfg.get("repo_dir"))
        # Prepare services and links
        links = {}
        for srv_aliace, srv in self.services.items():
            srv.run_service()
            links[srv.name] = srv_aliace
        # Prepare pipeline
        for stage in self.pipeline:
            for job in stage.get('jobs'):
                jcfg = job.get('cfg')
                # Add links to services
                jcfg['links'] = links
                # Setup work dir in container
                jcfg['work_dir'] = jcfg.get('work_dir', '/build')
                jcfg['volumes'] = {
                    self.cfg.get("repo_dir"): jcfg.get('work_dir')}
                # Prepare artifacts directory
                for artf in jcfg.get('artifacts', []):
                    artf_dir = os.path.join(
                        self.global_cfg.get('ARTIFACTS_DIR'),
                        self.cfg.get("name"),
                        stage.get('name'),
                        artf)
                    os.makedirs(artf_dir)
                    LOG.info("Create artifacts dir: %s", artf_dir)
                    jcfg['volumes'][artf_dir] = os.path.join(
                        jcfg.get('work_dir'), artf)

    @staticmethod
    def _worker(job):
        try:
            with Docker(job.get('name'), job.get('cfg')) as j:
                # Wait job execute
                j.run()
                # Store results
                return {"job": job.get('name'),
                        "logs": j.logs,
                        "errors": j.errors,
                        "status": j.status}
        except JobError as err:
            LOG.error("Job %s error: %s", job.get('name'), str(err))
            return {"job": job.get('name'),
                    "logs": None,
                    "errors": str(err),
                    "status": JobStatus.FAIL}

    def start(self):
        """Start each one stage, and jobs in it. Collect and check results"""
        results = {}
        for stage in self.pipeline:
            stage_name = stage.get('name')
            stage_jobs = stage.get('jobs')
            LOG.info("Run stage '%s'", stage_name)
            LOG.debug("Jobs: %s", PP.pformat(stage_jobs))
            # Make the Pool of workers
            pool = Pool(len(stage_jobs))
            # Run jobs
            jobs_results = pool.map(self._worker, stage_jobs)
            # Close the pool and wait for the work to finish
            pool.close()
            pool.join()
            # Store jobs results
            results[stage_name] = jobs_results
            # Check stage status
            stage_fail = False
            for res in jobs_results:
                if res is None:
                    stage_fail = True
                    continue
                if res.get('status') is not JobStatus.SUCCESS:
                    stage_fail = True
            if stage_fail:
                break
        return results

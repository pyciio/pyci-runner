"""
Docker job module.
"""

import os
import re
import pwd
import grp
import time
import logging
import docker
from build.job import JobStatus, JobError, JobResult

LOG = logging.getLogger('pyci-worker')


class Docker(JobResult):
    """Docker job class"""

    def __init__(self, job_name_, job_cfg_):
        self.client = docker.APIClient(base_url='unix://var/run/docker.sock')
        self.name = job_name_
        self.cfg = job_cfg_
        self.container = None

    def __enter__(self):
        self.create()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.remove()

    @staticmethod
    def get_status(status):
        """
        Convert docker status string to JobStatus type.

        All Docker container statuses:
        https://github.com/docker/docker/blob/b44b5bbc8ba48f50343602a21e7d44c017c1e23d/container/state.go#L41
        """
        if "Exited" in status:
            res = re.match(r'Exited \((\d+)\).*', status)
            if res is not None:
                # Bash exit codes
                # http://www.tldp.org/LDP/abs/html/exitcodes.html
                exit_code = int(res.group(1))
                if exit_code == 0:
                    return JobStatus.SUCCESS
                elif 128 < exit_code <= 137:
                    return JobStatus.ABORTED
        return JobStatus.FAIL

    def _stop_container(self, container_id):
        """Stop container"""
        self.client.stop(container_id, timeout=2)
        status = self.client.containers(all=True,
                                        filters={"id": container_id})
        LOG.info("Stoped container %s with status %s", container_id,
                 status[0].get('Status'))

    def _logs_container(self, container_id):
        """Get container logs"""
        return self.client.logs(container_id)

    def _state_container(self, container_id):
        """Get state and status of container"""
        cont_list = self.client.containers(all=True,
                                           filters={"id": container_id})
        if len(cont_list) == 0:
            return None, None
        return cont_list[0].get('State'), cont_list[0].get('Status')

    def _wait_container(self, container_id):
        """
        Wait job in container.

        Return:
            status, logs, errors
        """
        LOG.info("Wait container %s....", container_id)
        ticker = 0
        while True:
            # Check current status of container
            try:
                state, status_str = self._state_container(container_id)
                # Container killed
                if state is None:
                    return (JobStatus.ABORTED,
                            None,
                            "Container killed other programm")
                # Job timeout
                elif ticker == self.cfg.get('timeout', 60):
                    self._stop_container(container_id)
                    return (JobStatus.TIMEOUT,
                            self._logs_container(container_id),
                            "Job timeout")
                # Normal exit
                elif state != "running":
                    return (self.get_status(status_str),
                            self._logs_container(container_id), None)
                else:
                    # Wait 1 second...
                    LOG.debug("State: %s Status: %s", state, status_str)
                    time.sleep(1)
                    ticker += 1
            except docker.errors.APIError as err:
                return JobStatus.ABORTED, None, str(err)

    def _prapare_image(self):
        """
        Prepare docker image.
        If image not exist, pull it.
        """
        img = self.cfg.get('image', 'busybox:latest')
        if ":" not in img:
            img += ":latest"
        images = self.client.images(name=img)
        if len(images) == 0:
            LOG.info("Pulling image %s....", img)
            try:
                self.client.pull(img)
            except docker.errors.APIError as err:
                raise JobError(
                    "Pull image {0} error '{1}'".format(img, err))
        return img

    def _get_commands(self):
        # make commands
        commands = ""
        for i, cmd in enumerate(self.cfg.get('commands', [])):
            if i != 0:
                commands += ' && '
            commands += 'echo "$ {0}" && {0}'.format(cmd)
        commands = commands.replace('"', '\\"')
        # Add local user to container fro fix permissions
        cur_pwd = pwd.getpwuid(os.getuid())
        cur_grp = grp.getgrgid(cur_pwd[3])
        perms = ["groupadd -g {1} -r {0}".format(cur_grp[0], cur_grp[2]),
                 "useradd -r -g {1} -u {2} {0}".format(cur_pwd[0],
                                                       cur_grp[0],
                                                       cur_pwd[2]),
                 "chown {0}:{1} -R {2}/*".format(cur_pwd[0],
                                                 cur_grp[0],
                                                 self.cfg.get('work_dir'))]
        commands += " && " + "; ".join(perms)
        # wrap commands in shell
        commands = 'sh -c "' + commands + '"'
        return commands

    def _get_volumes(self):
        return [v for v, _ in self.cfg.get('volumes').items()]

    def _get_host_conf(self):
        binds = [v + ":" + b for v, b in self.cfg.get('volumes').items()]
        return self.client.create_host_config(binds=binds,
                                              links=self.cfg.get('links', []))

    def create(self, cont_cfg=None):
        """Create container from job config"""
        if cont_cfg is None:
            cont_cfg = {
                "image": self._prapare_image(),
                "command": self._get_commands(),
                "working_dir": self.cfg.get('work_dir'),
                "volumes": self._get_volumes(),
                "environment": self.cfg.get("environment"),
                "host_config": self._get_host_conf()
            }
        cont = self.client.create_container(**cont_cfg)
        self.container = cont.get("Id")
        LOG.info("Create container %s", self.container)

    def remove(self):
        """Remove job container"""
        if self.container is not None:
            state = self.client.containers(all=True,
                                           filters={"id": self.container})
            if len(state) > 0:
                LOG.info("Remove container %s", self.container)
                self.client.remove_container(
                    self.container, force=True, v=True)

    def run(self):
        """Run job in container and wait result"""
        LOG.info("Start container %s", self.container)
        try:
            self.client.start(self.container)
        except docker.errors.APIError as err:
            raise JobError(str(err))
        # Wait and save results
        self.status, self.logs, self.errors = self._wait_container(
            self.container)

    def run_service(self):
        """Create and start "service" container"""
        cont_cfg = {
            "name": self.name,
            "image": self._prapare_image(),
            "environment": self.cfg.get("environment")
        }
        self.create(cont_cfg)
        LOG.info("Start service container %s (%s)", self.name, self.container)
        self.client.start(self.container)

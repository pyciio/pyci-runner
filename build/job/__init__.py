"""
Module with job runners.
"""


class JobStatus(object):  # pylint: disable=too-few-public-methods
    """Job status enum"""
    SUCCESS = 0
    FAIL = 1
    ABORTED = 2
    TIMEOUT = 3


class JobResult(object):  # pylint: disable=too-few-public-methods
    """Parent class for all runner classes"""
    logs = None
    status = JobStatus.SUCCESS
    errors = None


class JobError(Exception):
    """Exception for error in runner"""
    pass

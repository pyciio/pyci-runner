"""
Configuration file with global settings
"""

import os

# pylint: disable=too-few-public-methods


class Config(object):
    """Default settings"""
    PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
    DEBUG = False
    TESTING = False
    # API server
    API_SERVER = "localhost:8080"
    REGISTER_TOKEN = ""
    # Docker
    DOCKER_URL = "unix://var/run/docker.sock"
    ARTIFACTS_DIR = "/tmp/artifacts"
    PARALLELS = 5
    # Logging
    LOG_FMT = "%(levelname)s %(message)-50s %(threadName)s " \
              "%(processName)s %(datetime)s"
    LOG_COLOR = True

# pylint: disable=too-few-public-methods


class ProductionConfig(Config):
    """Production settings"""
    pass

# pylint: disable=too-few-public-methods


class DevelopmentConfig(Config):
    """Development settings"""
    DEBUG = True

# pylint: disable=too-few-public-methods


class TestingConfig(Config):
    """Testing settings"""
    TESTING = True

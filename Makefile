.PHONY: env tests

VIRTUALENV := $(shell command -v virtualenv --version 2> /dev/null)
VENV_DIR = venv
OS_TYPE := $(shell uname -s)

env: ${VENV_DIR}
	@echo "OS requirements: python-dev python3-dev"
ifndef VIRTUALENV
	$(error "command 'virtualenv' is not available! Please install python-virtualenv")
	exit 1
endif
	@echo "Install env for python2...."
	@virtualenv -p python2 ${VENV_DIR}/py2
	${VENV_DIR}/py2/bin/pip install -r requirements/python2.txt
	@echo "Install env for python2...."
	@virtualenv -p python3 ${VENV_DIR}/py3
	${VENV_DIR}/py3/bin/pip install -r requirements/python3.txt

tests:
	@tox

lint:
	@tox -e py35-lint

coverage:
	@tox -e py35-coverage

coverage-report: coverage
	./.tox/coverage/bin/coverage html
ifeq ($(OS_TYPE),Linux)
	xdg-open /tmp/pyci-htmlcov/index.html
endif
ifeq ($(OS_TYPE),Darwin)
	open /tmp/pyci-htmlcov/index.html
endif

${VENV_DIR}:
	mkdir -p $@

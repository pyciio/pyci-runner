"""
Agent module for running builds
"""

import time
import logging
from multiprocessing import Pool

from build import Build

LOG = logging.getLogger('pyci-worker')

# pylint: disable=too-few-public-methods


class Agent:
    """
    Agent class, run parallel builds (get option config["PARALLELS"])
    """

    def __init__(self, global_cfg_, builds_):
        self.global_cfg = global_cfg_
        self.builds = builds_

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def _worker(self, build_cfg):
        LOG.info("Build %s run", build_cfg.get("name"))
        with Build(self.global_cfg, build_cfg) as build:
            return {'name': build_cfg.get('name'), 'stages': build.start()}

    def start(self):
        """
        Run 'count_proc' parallel builds
        """
        results = []
        count_proc = self.global_cfg.get('PARALLELS')
        # Run jobs
        i = 0
        while i < len(self.builds):
            part_builds = self.builds[i:min(len(self.builds), i + count_proc)]
            pool = Pool(processes=count_proc)
            results += pool.map(self._worker, part_builds)
            # Close the pool and wait for the work to finish
            pool.close()
            pool.join()
            i += count_proc
        return results

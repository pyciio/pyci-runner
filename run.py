"""
Run pyci-worker as daemon

docker run --rm -it --name test \
-v /tmp/tmpyy18d91t:/go/src/github.com/velp/go-cfgreader \
-w /go/src/github.com/velp/go-cfgreader golang:latest sh
"""

import sys
import json
import time
import logging

from agent import Agent
import helpers
from helpers.logger import LogrusFormatter
import config

# pylint: disable=invalid-name
if __name__ == "__main__":
    global_cfg = helpers.config_from_object(config.DevelopmentConfig)

    logger = logging.getLogger('pyci-worker')
    hdlr = logging.StreamHandler(sys.stdout)
    fmtr = LogrusFormatter(colorize=global_cfg.get("LOG_COLOR"),
                           fmt=global_cfg.get("LOG_FMT"))
    hdlr.setFormatter(fmtr)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)

    # Get builds configs
    with open('./test.json') as cfg:
        build_cfg1 = json.load(cfg)
    with open('./test.json') as cfg:
        build_cfg2 = json.load(cfg)
    build_cfg1['name'] += "-1-" + str(int(time.time()))
    build_cfg2['name'] += "-2-" + str(int(time.time()))
    builds = [build_cfg1, build_cfg2]
    # Run agent
    with Agent(global_cfg, builds) as a:
        buils_results = a.start()
    # Print results
    for b in buils_results:
        print("======> Build '{0}' results:".format(b['name']))
        for stage, stage_res in b['stages'].items():
            print("====> Stage '{0}' results:".format(stage))
            for job_res in stage_res:
                if job_res is not None:
                    print("==> Job '{0}' results:".format(job_res['job']))
                    print(str(job_res['logs']).replace('\\n', '\n'))

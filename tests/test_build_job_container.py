"""
Unit tests for module build.job.container
"""
import os
import pwd
import grp
import unittest
import docker
try:
    from unittest import mock
except ImportError:  # pragma: no cover
    import mock

from tests.helpers import fake_api

from build.job import JobError, JobStatus
from build.job.container import Docker


def get_commands():
    """
    Get control results for function
    buil.job.container.Docker._get_commands()
    """
    cmd = ('''sh -c 'echo "$ id" && id && '''
           '''echo "$ pwd" && pwd && '''
           '''groupadd -g {gid} -r {gname}; '''
           '''useradd -r -g {gname} -u {uid} {uname}; '''
           '''chown {uname}:{gname} -R None/*\'''')
    cur_pwd = pwd.getpwuid(os.getuid())
    cur_grp = grp.getgrgid(cur_pwd[3])
    return cmd.format(gid=cur_grp[2],
                      uid=cur_pwd[2],
                      gname=cur_grp[0],
                      uname=cur_pwd[0]).replace('"', '\\"').replace("'", '"')


# pylint: disable=protected-access
class MockDockerTest(unittest.TestCase):
    """
    Testing class build.job.container.Docker with mocking.
    """

    def setUp(self):
        self.patcher = mock.patch('docker.APIClient')
        mock_docker_client = self.patcher.start()
        self.docker_mock = mock_docker_client()
        # Fake docker APIClient functions
        self.docker_mock.containers = fake_api.containers
        self.docker_mock.create_container = fake_api.create_container
        self.docker_mock.images = fake_api.images
        self.docker_mock.pull = fake_api.pull
        self.docker_mock.logs = fake_api.logs
        self.docker_mock.create_host_config = fake_api.create_host_config

    def tearDown(self):
        self.patcher.stop()

    def test_prepare_image(self):
        """Test function Docker._prepare_image()"""
        inst = Docker(fake_api.FAKE_JOB_NAME, fake_api.FAKE_JOB_CFG)
        # Test good image
        self.assertEqual(inst._prapare_image(), 'busybox:latest')
        # Test with latest
        inst.cfg['image'] = 'busybox:latest'
        self.assertEqual(inst._prapare_image(), 'busybox:latest')
        # Test pull image
        inst.cfg['image'] = 'badimage'
        with self.assertRaises(JobError):
            inst._prapare_image()
        # Test default image
        del inst.cfg['image']
        self.assertEqual(inst._prapare_image(), 'busybox:latest')

    def test_get_commands(self):
        """Test function Docker._get_commands()"""
        inst = Docker(fake_api.FAKE_JOB_NAME, fake_api.FAKE_JOB_CFG)
        # Test make commands
        self.assertEqual(inst._get_commands(), get_commands())

    def test_logs_container(self):
        """Test function Docker._logs_container()"""
        inst = Docker(fake_api.FAKE_JOB_NAME, fake_api.FAKE_JOB_CFG)
        self.assertEqual(inst._logs_container(fake_api.FAKE_CONTAINER_ID),
                         b'logs\n')

    def test_get_status(self):
        """Test function Docker._get_status()"""
        # Test Exited good code
        self.assertEqual(Docker.get_status(
            "Exited (0) sec"), JobStatus.SUCCESS)
        # Test Exited bad code
        self.assertEqual(Docker.get_status("Exited (1) sec"), JobStatus.FAIL)
        # Test Exited abort code
        self.assertEqual(Docker.get_status(
            "Exited (137) sec"), JobStatus.ABORTED)
        # Test bad statuses
        self.assertEqual(Docker.get_status("bad status"), JobStatus.FAIL)
        self.assertEqual(Docker.get_status("Exited"), JobStatus.FAIL)

    def test_state_container(self):
        """Test function Docker._state_container()"""
        inst = Docker(fake_api.FAKE_JOB_NAME, fake_api.FAKE_JOB_CFG)
        # Good status
        self.assertEqual(inst._state_container(fake_api.FAKE_CONTAINER_ID),
                         ('running', 'Up 1 seconds'))
        # Status not exist container
        self.assertEqual(inst._state_container("bad"),
                         (None, None))

    def test_get_volumes(self):
        """Test function Docker._get_volumes()"""
        inst = Docker(fake_api.FAKE_JOB_NAME, fake_api.FAKE_JOB_CFG)
        self.assertEqual(inst._get_volumes(), ['/var/lib/mysql'])

    def test_get_host_conf(self):
        """Test function Docker._get_host_conf()"""
        inst = Docker(fake_api.FAKE_JOB_NAME, fake_api.FAKE_JOB_CFG)
        self.assertEqual(inst._get_host_conf().get('binds'),
                         ['/var/lib/mysql:/mysql'])

    def test_stop_container(self):
        """Test function Docker._stop_container()"""
        inst = Docker(fake_api.FAKE_JOB_NAME, fake_api.FAKE_JOB_CFG)
        self.assertEqual(inst._stop_container(
            fake_api.FAKE_CONTAINER_ID), None)

    def test_context(self):
        """Test context of Docker"""
        with Docker(fake_api.FAKE_JOB_NAME, fake_api.FAKE_JOB_CFG) as inst:
            self.assertNotEqual(inst.container, None)
        self.assertEqual(self.docker_mock.remove_container.call_count, 1)

    @mock.patch('time.sleep', return_value=None)
    # pylint: disable=unused-argument
    def test_wait_container(self, *args, **kwargs):
        """Test function Docker._wait_container()"""
        inst = Docker(fake_api.FAKE_JOB_NAME, fake_api.FAKE_JOB_CFG)
        # Killed container
        with mock.patch.object(inst, '_state_container') as mock_docker:
            mock_docker.return_value = (None, None)
            res = inst._wait_container(fake_api.FAKE_CONTAINER_ID)
            self.assertEqual(res, (JobStatus.ABORTED, None,
                                   'Container killed other programm'))
        # Normal exit
        with mock.patch.object(inst, '_state_container') as mock_docker:
            mock_docker.return_value = ('exit', 'Exited (0)')
            res = inst._wait_container(fake_api.FAKE_CONTAINER_ID)
            self.assertEqual(res, (JobStatus.SUCCESS, b'logs\n', None))
        # Timeout
        with mock.patch.object(inst, '_state_container') as mock_docker:
            inst.cfg['timeout'] = 1
            mock_docker.return_value = ('running', 'Up')
            res = inst._wait_container(fake_api.FAKE_CONTAINER_ID)
            self.assertEqual(
                res, (JobStatus.TIMEOUT, b'logs\n', 'Job timeout'))
        # inst.error Exception
        with mock.patch.object(inst, '_state_container') as mock_docker:
            mock_docker.side_effect = docker.errors.APIError("error")
            res = inst._wait_container(fake_api.FAKE_CONTAINER_ID)
            self.assertEqual(res, (JobStatus.ABORTED, None, 'error'))

    def test_run(self):
        """Test function Docker.run()"""
        inst = Docker(fake_api.FAKE_JOB_NAME, fake_api.FAKE_JOB_CFG)
        # normal run
        with mock.patch.object(inst, '_wait_container') as mock_docker:
            mock_docker.return_value = (JobStatus.SUCCESS, b'logs\n', None)
            inst.run()
            mock_docker.assert_called_with(None)
        # inst.error Exception
        self.docker_mock.start.side_effect = docker.errors.APIError("error")
        with self.assertRaises(JobError):
            inst.run()
        self.docker_mock.start.reset_mock()

    def test_run_services(self):
        """Test function Docker.run_service()"""
        inst = Docker(fake_api.FAKE_JOB_NAME, fake_api.FAKE_JOB_CFG)
        with mock.patch.object(inst, 'create') as mock_docker:
            inst.run_service()
            self.assertTrue(mock_docker.called)
            self.assertEqual(self.docker_mock.start.call_count, 1)

"""
Unit tests for build.__init__.py
"""

import unittest
try:
    from unittest import mock
except ImportError:  # pragma: no cover
    import mock
import tests.factory.build
from build import Build
from build.job import JobStatus
import build.job.container

# pylint: disable=protected-access


class TestBuild(unittest.TestCase):
    """
    Testing class build.Build with mocking.
    """

    def setUp(self):
        self.build_cfg = tests.factory.build.build_cfg()

    @mock.patch('build.Build._parse_build_cfg')
    def test_init(self, mock_parse):
        """Test function Build.__init__()"""
        mock_parse.return_value = [], {}
        # Nomal config
        Build(None, self.build_cfg)
        self.assertTrue(mock_parse.called)
        # Config without
        with self.assertRaises(Exception):
            Build(None, {})
        self.assertTrue(mock_parse.called)

    @mock.patch('build.Build._parse_build_cfg')
    @mock.patch('build.Build._prepare')
    @mock.patch('shutil.rmtree', return_value=None)
    @mock.patch('build.LOG')
    def test_context(self, mock_log, mock_rmtree, mock_prepare, mock_parse):
        """Test context of Build"""
        mock_docker = mock.Mock()
        mock_parse.return_value = [], {'srv': mock_docker}
        # Normal
        with Build(None, self.build_cfg) as _:
            self.assertTrue(mock_prepare.called)
        self.assertTrue(mock_docker.remove.called)
        # Bad repo_dir
        mock_rmtree.side_effect = OSError("error")
        with Build(None, self.build_cfg) as _:
            self.assertTrue(mock_prepare.called)
        self.assertTrue(mock_docker.remove.called)
        self.assertTrue(mock_log.error.called)

    @mock.patch('build.Build.__init__', return_value=None)
    def test_get_jobs(self, _):
        """Test function Build._get_jobs()"""
        # Normal
        inst = Build(None, None)
        jobs = inst._get_jobs({"test": {"stage": "start"}}, "start")
        self.assertEqual(jobs, [{'name': 'test', 'cfg': {'stage': 'start'}}])
        # Not found jobs for stage
        with self.assertRaises(Exception):
            inst._get_jobs({}, "start")

    @mock.patch('build.Build.__init__', return_value=None)
    @mock.patch('build.Build._get_jobs', return_value=[])
    @mock.patch('build.job.container.Docker.__init__', return_value=None)
    # pylint: disable=unused-argument
    def test_parse_build_cfg(self, mock_docker, mock_jobs, *args, **kwargs):
        """Test function Build._parse_build_cfg()"""
        stages = self.build_cfg.get("stages")
        services = self.build_cfg.get("services")
        inst = Build(None, None)
        pipe, srv = inst._parse_build_cfg(self.build_cfg)
        # Check call _get_jobs
        self.assertEqual(mock_jobs.call_count, len(stages))
        # Check create Docker services
        self.assertEqual(mock_docker.call_count, len(services))
        # Count stages
        self.assertEqual(len(pipe), len(stages))
        # Order stages
        for indx, stage_name in enumerate(stages):
            self.assertIn("name", pipe[indx])
            self.assertIn("jobs", pipe[indx])
            self.assertEqual(pipe[indx].get("name"), stage_name)
        # Count services
        self.assertEqual(len(srv), len(services))
        for aliace, _ in self.build_cfg.get('services').items():
            self.assertIn(aliace, srv)
            # Type
            self.assertIsInstance(srv.get(aliace), build.job.container.Docker)
        # 'stages' is string
        pipe, _ = inst._parse_build_cfg({"stages": "start", "pipeline": {}})
        # Count stages
        self.assertEqual(len(pipe), 1)
        self.assertEqual(pipe[0].get("name"), "start")

    @mock.patch('build.Build._parse_build_cfg')
    @mock.patch('tempfile.mkdtemp', return_value=None)
    @mock.patch('git.Repo.clone_from', return_value=None)
    @mock.patch('os.makedirs', return_value=None)
    def test_prepare(self, mock_makedirs, mock_git, mock_mkdtemp, mock_parse):
        """Test function Build._prepare()"""
        mock_docker = mock.Mock()
        pipe = [{"name": "start", "jobs": [{"cfg": {"artifacts": ["test"]}}]}]
        mock_parse.return_value = pipe, {'srv': mock_docker}
        inst = Build({"ARTIFACTS_DIR": "test"}, self.build_cfg)
        inst._prepare()
        # Not create tmpdir
        self.assertFalse(mock_mkdtemp.called)
        # Clone git
        mock_git.assert_called_with(self.build_cfg.get(
            "repo"), self.build_cfg.get("repo_dir"))
        # Run services
        self.assertEqual(mock_docker.run_service.call_count, 1)
        # Create artifacts dir
        self.assertEqual(mock_makedirs.call_count, 1)
        mock_makedirs.assert_called_with(
            "test/{0}/start/test".format(self.build_cfg.get("name")))
        # Build without repo_dir
        empty_cfg = {"name": "", "repo": "", "stages": [], "pipeline": {}}
        inst = Build({"ARTIFACTS_DIR": "test"}, empty_cfg)
        inst._prepare()
        # Create tmpdir
        self.assertTrue(mock_mkdtemp.called)

    @mock.patch('build.Build.__init__', return_value=None)
    @mock.patch('build.job.container.Docker.__init__', return_value=None)
    @mock.patch('build.job.container.Docker.remove')
    @mock.patch('build.job.container.Docker.__enter__')
    @mock.patch('build.LOG')
    # pylint: disable=unused-argument
    def test_worker(self, mock_log, mock_docker_enter, *args, **kwargs):
        """Test function Build._worker()"""
        mock_context = mock_docker_enter.return_value
        mock_context.run.return_value = None
        mock_context.logs = "logs"
        mock_context.errors = None
        mock_context.status = "status"
        inst = Build(None, None)
        # Normal job
        res = inst._worker({"name": "test", "cfg": {}})
        mock_context.run.assert_called_with()
        self.assertEqual(res, {'job': 'test', 'logs': 'logs',
                               'errors': None, 'status': 'status'})
        # JobError
        mock_context.run.side_effect = build.job.JobError("error")
        res = inst._worker({"name": "test", "cfg": {}})
        mock_context.run.assert_called_with()
        self.assertEqual(res, {'job': 'test', 'logs': None,
                               'errors': 'error', 'status': JobStatus.FAIL})
        # Check logging after exception
        self.assertTrue(mock_log.error.called)

    @mock.patch('build.Build.__init__', return_value=None)
    @mock.patch('build.Build.pipeline', new_callable=mock.PropertyMock)
    @mock.patch('build.Pool')  # It's multiprocessing.dummy.Pool() function
    # pylint: disable=unused-argument
    def test_start(self, mock_pool, mock_pipe, *args, **kwargs):
        """Test function Build.start()"""
        stages = 3
        jobs = 3
        mock_pipe.return_value = [{"name": s, "jobs": []}
                                  for s in range(stages)]
        mock_pool.return_value = mock.Mock()
        job_res = [{"job": j,
                    "logs": "",
                    "errors": None,
                    "status": JobStatus.SUCCESS} for j in range(jobs)]
        mock_pool.return_value.map.return_value = job_res
        inst = Build(None, None)
        # Normal
        res = inst.start()
        self.assertEqual(len(res), stages)
        for num in range(stages):
            self.assertIn(num, res)
            self.assertEqual(len(res.get(num)), jobs)
            for j in range(jobs):
                self.assertEqual(res.get(num)[j].get("job"), j)
                self.assertEqual(res.get(num)[j].get("logs"), "")
                self.assertEqual(res.get(num)[j].get("errors"), None)
                self.assertEqual(res.get(num)[j].get(
                    "status"), JobStatus.SUCCESS)
        # One job failed
        mock_pool.return_value.map.return_value[0]["status"] = JobStatus.FAIL
        res = inst.start()
        self.assertEqual(len(res), 1)
        self.assertEqual(len(res.get(0)), 3)
        # Result None
        mock_pool.return_value.map.return_value[0] = None
        res = inst.start()
        self.assertEqual(len(res), 1)
        self.assertEqual(len(res.get(0)), 3)

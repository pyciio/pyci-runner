"""
Fake api functions for mocking docker.APIClient
"""

import docker

FAKE_CONTAINER_ID = '3cc2351ab11b'
FAKE_IMAGE_ID = 'e9aa60c60128'
FAKE_JOB_VOLUME = "/var/lib/mysql"
FAKE_JOB_BIND = "/mysql"
FAKE_JOB_NAME = "test"
FAKE_JOB_CFG = {
    "image": "busybox",
    "commands": ["id", "pwd"],
    "volumes": {FAKE_JOB_VOLUME: FAKE_JOB_BIND}
}


def images(name):
    """Make result for fake docker.APIClient.images()"""
    if name == 'busybox:latest':
        return [{
            'Id': FAKE_IMAGE_ID,
            'Created': '2 days ago',
            'Repository': 'busybox',
            'RepoTags': ['busybox:latest', 'busybox:1.0'],
        }]
    else:
        return []


def pull(name):
    """Make result for fake docker.APIClient.pull()"""
    if name != 'busybox:latest':
        raise docker.errors.APIError("Bad image")


def logs(container):
    """Make result for fake docker.APIClient.logs()"""
    if container == FAKE_CONTAINER_ID:
        return b'logs\n'

# pylint: disable=unused-argument


def containers(*args, **kwargs):
    """Make result for fake docker.APIClient.containers()"""
    if kwargs['filters'].get('id') == FAKE_CONTAINER_ID:
        return [{
            'Id': FAKE_CONTAINER_ID,
            'Image': 'busybox:latest',
            'Name': 'foobar',
            "State": "running",
            "Status": "Up 1 seconds",
        }]
    else:
        return []

# pylint: disable=unused-argument


def create_host_config(*args, **kwargs):
    """Make result for fake docker.APIClient.create_host_config()"""
    return kwargs

# pylint: disable=unused-argument


def create_container(*args, **kwargs):
    """Make result for fake docker.APIClient.create_container()"""
    return {'Id': FAKE_CONTAINER_ID}

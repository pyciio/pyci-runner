"""
Factory module for config build.Build class
"""

import os
import json

CFG_PATH = os.path.dirname(os.path.abspath(__file__))


def build_cfg():
    """Load config for test case from file cfg.json"""
    with open('%s/cfg.json' % CFG_PATH) as cfg:
        return json.load(cfg)
